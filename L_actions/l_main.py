# coding: utf-8
from Kuznechik.R_actions import r_main



# Функция L преобразования -> L conversion function
# Принимает s16_32 -> Input s16_32
# Отдаёт s16_32 -> Output s16_32
def l_conversion(string):
    result = string
    for i in range(16):
        result = r_main.r_conversion(result)
    return result


# Функция обратная L преобразования -> reverse L conversion function
def rl_conversion(string):
    result = string
    for i in range(16):
        result = r_main.rr_conversion(result)
    return result

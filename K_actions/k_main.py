# coding: utf-8
from Kuznechik.K_actions import k_functions
from Kuznechik.K_actions import c_action


# Функция K регенерации -> Raund keys generation function
def k_conversion(keys):
    # Выроботка констант -> generation const C
    mas_c = c_action.c_conversion()
    # Состовляющие мастер ключа -> master key
    k1 = keys[:32]
    k2 = keys[32:]
    # Выроботка подкючей 3 и 4 -> 3 and 4 raund key
    k34 = k_functions.get_ki(mas_c[:8], k1, k2)
    k3 = k34[0]
    k4 = k34[1]
    # Выроботка подкючей 5 и 6 -> 5 and 6 raund key
    k56 = k_functions.get_ki(mas_c[8:16], k3, k4)
    k5 = k56[0]
    k6 = k56[1]
    # Выроботка подкючей 7 и 8 -> 7 and 8 raund key
    k78 = k_functions.get_ki(mas_c[16:24], k5, k6)
    k7 = k78[0]
    k8 = k78[1]
    # Выроботка подкючей 9 и 10 -> 9 and 10 raund key
    k910 = k_functions.get_ki(mas_c[24:], k7, k8)
    k9 = k910[0]
    k10 = k910[1]
    return [k1, k2, k3, k4, k5, k6, k7, k8, k9, k10]

# coding: utf-8
from Kuznechik.L_actions import l_main
from Kuznechik.S_actions import associative_table


# Функция С регенерации -> C consts generation functions
def c_conversion():
    result = []
    # D → V128
    mas_v128 = [d_to_v128(i + 1) for i in range(32)]
    # Перевод строки из 128 2-ых обозначений в 32 16-ый ->
    # transfer from string with 128 char binary notation to 32 char hex notation
    mas_s32 = [v128_to_s32(i) for i in mas_v128]
    for i in mas_s32:
        c = l_main.l_conversion(i)
        result.append(c)
    return result


# Перевод из десятичного в 128 2-ых значение ->
# transfer from dec int to string with 128 char binary notation
def d_to_v128(d):
    result = ""
    for i in range(128):
        adder = d%2
        result = str(adder) + result
        d = d//2
    return result


# Перевод из 128 2-ых значений в десятичное ->
# transfer from string with 128 char binary notation to dec int
def v128_to_d(v128):
    result = 0
    rv128 = v128[::-1]
    for i in range(len(rv128)):
        if rv128[i] == "1":
            result = result + (2**i)
    return result


# Перевод строки из 128 2-ых обозначений в 32 16-ый ->
# transfer from string with 128 char binary notation in 32 char hex notation
def v128_to_s32(v128):
    mas_v4 = [v128[i * 4:i * 4 + 4] for i in range(32)]
    mas_s1 = [associative_table.astv_ttos_16th[i] for i in mas_v4]
    return "".join(mas_s1)


# Перевод строки из 32 16-ых обозначений в 128 2-ых ->
# transfer from string with 32 char hex notation to string with 128 char binary notation
def s32_to_v128(s32):
    mas_v4 = [associative_table.astv_stot_16th[i] for i in s32]
    return "".join(mas_v4)

# Перевод строки из десятичного в 32 16-ый ->
# transfer from dec int to string with 32 char hex notation
def d_to_s32(d):
    return v128_to_s32(d_to_v128(d))


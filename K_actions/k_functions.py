# coding: utf-8
from Kuznechik.F_actions import f_main


# Получение итерационного подключа -> get 1 raund key
def get_ki(mas_c, k1, k2):
    result = [k1, k2]
    for i in mas_c:
        result = f_main.f_conversion(i, result[0], result[1])
    return result
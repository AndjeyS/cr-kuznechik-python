# coding: utf-8
from Kuznechik.K_actions import c_action


# Функция X преобразования -> X transformation function
# Принимает s16_32 -> Input s16_32
# Отдаёт s16_32 -> Output s16_32
def x_conversion(k, a):
    result = ""
    k = c_action.s32_to_v128(k)
    a = c_action.s32_to_v128(a)
    if len(k) == len(a):
        for i in range(128):
            if k[i] == a[i]:
                result = result + "0"
            else:
                result = result + "1"
        return c_action.v128_to_s32(result)
    else:
        return 0


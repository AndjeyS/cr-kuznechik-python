# coding: utf-8
from Kuznechik.L_actions import l_main
from Kuznechik.S_actions import s_main
from Kuznechik.K_actions import k_main
from Kuznechik.X_actions import x_main


# Шифрование -> Encrypt
def encryption(text, key):
    # Получаю раyндовые ключи -> Get raunds key
    keys = k_main.k_conversion(key)
    result = text
    # Произвожу десять раундов -> Start 10 raunds
    for i in range(len(keys)-1):
        x = x_main.x_conversion(keys[i], result)
        s = s_main.s_conversion(x)
        result = l_main.l_conversion(s)
    main_result = x_main.x_conversion(result, keys[len(keys)-1])
    return main_result


# Дешифрование -> Decrypt
def decryption(text, key):
    # Получаю раyндовые ключи -> Get raunds key
    keys = k_main.k_conversion(key)
    keys = keys[::-1]
    result = text
    # Произвожу десять раундов -> Start 10 raunds
    for i in range(len(keys)-1):
        x = x_main.x_conversion(keys[i], result)
        l = l_main.rl_conversion(x)
        result = s_main.rs_conversion(l)
    main_result = x_main.x_conversion(result, keys[len(keys)-1])
    return main_result

# coding: utf-8
from Kuznechik.L_actions import l_main
from Kuznechik.S_actions import s_main
from Kuznechik.X_actions import x_main


# Функция F преобразования -> F transformation function
def f_conversion(k, a1, a0):
    x = x_main.x_conversion(k, a1)
    s = s_main.s_conversion(x)
    l = l_main.l_conversion(s)
    return [x_main.x_conversion(l, a0), a1]


# Kuznechik Python

Python implementation of GOST Kuznechik cipher.

## How to use?

```python
# 1) Import main func from Dec_Enc_algoritms
from Kuznechik.Dec_Enc_algoritms import dec_enc_func
# 2) Import params for algorithm
from Kuznechik.Main_code import params

# 3) Init input and key like str for future work
state_input = "212533445f667a00dffeed3cc0baa998"
main_key = "cdba0078f5a5fca204b0d8fb943475cf7d6115a6b79f87854757f437a995a78d"

# 5) Use corresponding dec/enc function
output = dec_enc_func.encryption(state_input, main_key)
test_input = dec_enc_func.decryption(output, main_key)
```

## Where did i get Kuznechik description?

Paper name: "КРИПТОГРАФИЧЕСКАЯ ЗАЩИТА ИНФОРМАЦИИ"

Source: http://wwwold.tc26.ru/standard/gost/GOST_R_3412-2015.pdf

## Development

git clone git@bitbucket.org:AndjeyS/cr-kuznechik-python.git

# coding: utf-8
from Kuznechik.Dec_Enc_algoritms import dec_enc_func
from Kuznechik.Main_code import params
import time


# Параметры -> Params for enc/dec
input = params.input
test_output = params.test_output
main_key = params.main_key
mn = "cdba0078f5a5fca204b0d8fb943475cf7d6115a6b79f87854757f437a995a78d"

# Шифрованеи и дешифрование -> Main enc/dec test
output = dec_enc_func.encryption(input, mn)
input_ = dec_enc_func.decryption(output, mn)